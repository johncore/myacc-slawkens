<?php
/**
 * Automatic PagSeguro payment system gateway.
 *
 * @name      myaac-pagseguro
 * @author    Ivens Pontes <ivenscardoso@hotmail.com>
 * @author    Slawkens <slawkens@gmail.com>
 * @website   github.com/slawkens/myaac-pagseguro
 * @website   github.com/ivenspontes/
 * @version   1.1.1
 */

$config['pagSeguro'] = array(
	'email' => 'renan.ulian@gmail.com',
	'environment' => 'production', // production, sandbox
	'token' => array(
		'production' => '3ce7e501-8f12-4315-a68c-8741f5ccca2553febefe4ea283a81ec85687e4166fd3ac53-0314-4fb7-b627-0c1b0906c436',
		'sandbox' => '3ce7e501-8f12-4315-a68c-8741f5ccca2553febefe4ea283a81ec85687e4166fd3ac53-0314-4fb7-b627-0c1b0906c436',
	),
	'urlRedirect' => '?subtopic=points&action=final', // default should be good
	'productName' => 'Coins', // or Premium Points
	'productValue' => 1.00,
	'doublePoints' => false, // should points be doubled? for example: for 5 points donated you become 10.
	'donationType' => 'points', // what should be added to player account? points/coins
	'options' => array( // cost => points/coins
		'10,00' => 10,
		'20,00' => 40,
		'30,00' => 60,
		'40,00' => 80,
		'50,00' => 100,
		'60,00' => 120,
		'70,00' => 140,
		'80,00' => 160,
		'90,00' => 180,
		'100,00' => 300,
	)
);
?>