-- Parser
dofile('data/modules/scripts/gamestore/init.lua')
-- Config
GameStore.Categories = {
	
	
		{
		name = "Warzave Items",
		state = GameStore.States.STATE_NONE,
        description = "Items exclusivos.",
		rookgaard = true,
		icons = {"Category_wz.png"},
		offers = {
			{name = "The Hat Warzave", thingId = 13947, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 43, icons = {"TheEpicWisdom.png"}, description = "(Arm:10, magic level +7, protection physical +8%, energy +15%, fire +20%)."},
			{name = "Warzave Boots", thingId = 2358, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"2358.png"}, description = "(Arm 2, Speed +30, Regen mana +100 / hp +80)."},
			
		}
	},
	
	{
		name = "Exercise Training",
		state = GameStore.States.STATE_NONE,
        description = "Items de treinamento.",
		rookgaard = true,
		icons = {"Category_Convenience.png"},
		offers = {
			{name = "Exercise Sword", thingId = 32384, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Exercise_Sword.png"}, description = "Exercise items training."},
			{name = "Exercise Axe", thingId = 32385, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Exercise_Axe.png"}, description = "Exercise items training."},
			{name = "Exercise Club", thingId = 32386, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Exercise_Club.png"}, description = "Exercise items training."},
			{name = "Exercise Bow", thingId = 32387, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Exercise_Bow.png"}, description = "Exercise items training."},
			{name = "Exercise Rod", thingId = 32389, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Exercise_Rod.png"}, description = "Exercise items training."},
			{name = "Exercise Wand", thingId = 32388, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Exercise_Wand.png"}, description = "Exercise items training."},
			
		}
	},
	
	{
		name = "Useful Things",
		state = GameStore.States.STATE_NONE,
        description = "Buy your character one or more of the helpful items offered here.",
		rookgaard = true,
		icons = {"Category_Convenience.png"},
		offers = {
			{name = "XP Boost 50%", type = GameStore.OfferTypes.OFFER_TYPE_EXPBOOST, price = 12, icons = {"XP_Boost_Icon.png"}, description = "Concede 50% de experiencia a mais por uma hora."},
		    {name = "Prey Bonus Reroll", count = 1, type = GameStore.OfferTypes.OFFER_TYPE_PREYBONUS, price = 2, icons = {"Product_UsefulThings_PreyBonusReroll.png"}},
			{name = "5x Prey Bonus Reroll", count = 5, type = GameStore.OfferTypes.OFFER_TYPE_PREYBONUS, price = 5, icons = {"Product_UsefulThings_PreyBonusReroll.png"}},
			{name = "Permanent Prey Slot", type = GameStore.OfferTypes.OFFER_TYPE_PREYSLOT, price = 10, icons = {"Product_UsefulThings_PermanentPreySlot.png"}},
			{name = "Temple Teleport", type = GameStore.OfferTypes.OFFER_TYPE_TEMPLE, price = 3, icons = {"Product_Transportation_TempleTeleport.png"}, description = "Te teleporta para o templo imediatamente. Só pode ser usado uma vez."},
			{name = "Gold Pounch", thingId = 26377, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 5, icons = {"Product_MagicCoinPurse.png"}, description = "Guarda todo o seu gold enquanto você tiver capacidade para carregar, facilitando sua vida."},
		    {name = "Stamina Refill", thingId = 12544, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 10, icons = {"12544.png"}, description = "Enchera toda a sua stamina. Apenas uma carga."},
			{name = "Skull Remover", thingId = 11144, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 10, icons = {"11144.png"}, description = "Remove seu Red skull ou Black skull."},
			{name = "Addon Doll", thingId = 8982, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"8982.png"}, description = "Com esse item voce pode escolher uma de nossas Addons, exceto a Mage Addon."},
			{name = "Montaria Doll", thingId = 9019, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"9019.png"}, description = "Com esse item voce pode escolher uma de nossas montarias, exceto a novas do update."},	
			{name = "Character Sex Change", thingId = 13030, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 25, icons = {"Product_CharacterSexChange.png"}, description = "Com esse item voce pode trocar de sexo."},	
			{name = "Character Name Change", type = GameStore.OfferTypes.OFFER_TYPE_NAMECHANGE, price = 25, icons = {"Product_CharacterNameChange.png"}},
	    	{name = "Sneaky Stabber of Eliteness", thingId = 10511, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price =  7, icons = {"Sneaky_Stabber_of_Eliteness.png"}},
	    	{name = "Squeezing Gear of Girlpower", thingId = 10513, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price =  7, icons = {"Squeezing_Gear_of_Girlpower.png"}},
		    {name = "Whacking Driller of Fate", thingId = 10515, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price =  7, icons = {"Whacking_Driller_of_Fate.png"}},
		}
	},
	
	{
		name = "Blessings",
		state = GameStore.States.STATE_NONE,
		icons = {"category_blessings.png"},
		rookgaard = true,
		offers = {
			{name = "All Regular Blessings", thingId = 9, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 7, icons = {"Product_Blessing_AllPvE.png"}},
			{name = "Twist of Fate", thingId = 1, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_Fate.png"}},
			{name = "The Wisdom of Solitude", thingId = 2, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_Solitude.png"}},
			{name = "The Spark of the Phoenix", thingId = 3, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_Phoenix.png"}},
			{name = "The Fire of the Suns", thingId = 4, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_Suns.png"}},
			{name = "The Spiritual Shielding", thingId = 5, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_Shielding.png"}},
			{name = "The Embrace of Tibia", thingId = 6, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_Tibia.png"}},
			{name = "Heart of the Mountain", thingId = 7, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_HeartOfTheMountain.png"}},
			{name = "Blood of the Mountain", thingId = 8, type = GameStore.OfferTypes.OFFER_TYPE_BLESSINGS, price = 2, icons = {"Product_Blessing_BloodOfTheMountain.png"}},
		}
	},
	
	{
		name = "Mounts",
		state = GameStore.States.STATE_NONE,
		icons = {"Category_Mounts.png"},
		rookgaard = true,
		offers = {
		    {name = "Widow Queen", thingId = 1, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 5, icons = {"o368.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "War Horse", thingId = 17, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 5, icons = {"o392.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Blazebringer", thingId = 9, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 5, icons = {"o376.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},				
			{name = "Prismatic Unicorn", thingId = 115, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"o1019.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Blazing Unicorn", thingId = 113, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"o1017.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Artic Unicorn", thingId = 114, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"o1018.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			
			{name = "Stampor", thingId = 11, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o378.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Uniwheel", thingId = 15, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o389.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Shadow Draptor", thingId = 24, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 12, icons = {"o427.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Ladybug", thingId = 27, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o447.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Manta Ray", thingId = 28, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o450.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Dragonling", thingId = 31, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o506.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Crimson Ray", thingId = 33, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o521.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Steelbeak", thingId = 34, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o522.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Tombstinger", thingId = 36, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o546.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Platesaurian", thingId = 37, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o547.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Ursagrondon", thingId = 38, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o548.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "The Hellgrip", thingId = 39, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 8, icons = {"o559.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},			
			{name = "Desert King", thingId = 41, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 8, icons = {"o572.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Azudocus", thingId = 44, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o621.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Carpacosaurus", thingId = 45, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o622.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Death Crawler", thingId = 46, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o624.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Flamesteed", thingId = 47, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o626.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Jade Lion", thingId = 48, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o627.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Jade Pincer", thingId = 49, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o628.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Nethersteed", thingId = 50, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o629.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Tempest", thingId = 51, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o630.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Winter King", thingId = 52, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o631.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Doombringer", thingId = 53, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o644.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Woodland Prince", thingId = 54, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o647.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Hailtorm Fury", thingId = 55, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o648.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Siegebreaker", thingId = 56, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o649.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Poisonbane", thingId = 57, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o650.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Blackpelt", thingId = 58, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o651.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Golden Dragonfly", thingId = 59, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o669.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Steel Bee", thingId = 60, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o670.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Copper Fly", thingId = 61, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o671.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Tundra Rambler", thingId = 62, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o672.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Highland Yak", thingId = 63, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o673.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Glacier Vagabond", thingId = 64, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o674.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Flying Divan", thingId = 65, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o688.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Magic Carpet", thingId = 66, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o689.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Floating Kashmir", thingId = 67, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o690.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Ringtail Waccoon", thingId = 68, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o691.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Night Waccoon", thingId = 69, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o692.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Emerald Waccoon", thingId = 70, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o693.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Glooth Glider", thingId = 71, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o682.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Shadow Hart", thingId = 72, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o685.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Black Stag", thingId = 73, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o686.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Emperor Deer", thingId = 74, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o687.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Flitterkatzen", thingId = 75, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o726.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Venompaw", thingId = 76, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o727.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Batcat", thingId = 77, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o728.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Sea Devil", thingId = 78, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o734.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Coralripper", thingId = 79, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o735.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Plumfish", thingId = 80, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o736.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Gorongra", thingId = 81, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o738.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Noctungra", thingId = 82, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o739.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Silverneck", thingId = 83, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o740.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Slagsnare", thingId = 84, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o761.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Nightstinger", thingId = 85, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o762.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Razorcreep", thingId = 86, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o763.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Rift Runner", thingId = 87, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o848.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Nightdweller", thingId = 88, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o849.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Frostflare", thingId = 89, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o850.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Cinderhoof", thingId = 90, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o851.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Mouldpincer", thingId = 91, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o868.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Bloodcurl", thingId = 92, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o869.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Leafscuttler", thingId = 93, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o870.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Sparkion", thingId = 94, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o883.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Swamp Snapper", thingId = 95, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o886.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Mould Shell", thingId = 96, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o887.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Reed Lurker", thingId = 97, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o888.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Neon Sparkid", thingId = 98, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o889.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Vortexion", thingId = 99, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o890.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Ivory Fang", thingId = 100, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o901.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Shadow Claw", thingId = 101, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o902.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			{name = "Snow Pelt", thingId = 102, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 10, icons = {"o903.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
		    
			{name = "Mole Mount", thingId = 119, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Mole.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Marsh Toad Mount", thingId = 120, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Marsh_Toad.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Sanguine Frog Mount", thingId = 121, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Sanguine_Frog.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Toxic Toad Mount", thingId = 122, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Toxic_Toad.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Ebony Tiger Mount", thingId = 123, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Ebony_Tiger.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Feral Tiger Mount", thingId = 124, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Feral_Tiger.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Jungle Tiger Mount", thingId = 125, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Jungle_Tiger.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Flying Book Mount", thingId = 126, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 20, icons = {"Flying_Book.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Tawny Owl Mount", thingId = 127, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Tawny_Owl.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Boreal Owl Mount", thingId = 128, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Boreal_Owl.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Snowy Owl Mount", thingId = 129, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 15, icons = {"Snowy_Owl.png"}, description = "Here you can purchase the Mount  for your character. Riding on a mount is not only cool, but also gives your character a speed boost."},
			
			{name = "Thornfire Wolf Mount", thingId = 150, type = GameStore.OfferTypes.OFFER_TYPE_MOUNT, price = 45, icons = {"Thornfire_Wolf.png"}, description = "+40 speed boost."},
			
		
		}
	},

	{
		name = "Outfits",
		state = GameStore.States.STATE_NONE,
		icons = {"Category_Outfits.png"},
		rookgaard = true,
		offers = {
		    {name = "Citizen Addon", thingId = {male=128,female=136}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 9, icons = {"f128.png", "f136.png"}},
			{name = "Hunter Addon", thingId = {male=129,female=137}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 9, icons = {"f129.png", "f137.png"}},
			{name = "Knight Addon", thingId = {male=139,female=131}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 9, icons = {"f139.png", "f131.png"}},
			{name = "Noblewoman Addon", thingId = {male=132,female=140}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f132.png", "f140.png"}},
			{name = "Warrior Addon", thingId = {male=134,female=142}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 13, icons = {"f134.png", "f142.png"}},
			{name = "Druid Addon", thingId = {male=144,female=148}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f144.png", "f148.png"}},
			{name = "Wizard Addon", thingId = {male=145,female=149}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f145.png", "f149.png"}},
			{name = "Beggar Addon", thingId = {male=153,female=157}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f153.png", "f157.png"}},
			{name = "Shaman Addon", thingId = {male=154,female=158}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f154.png", "f158.png"}},
			{name = "Norsewoman Addon", thingId = {male=251,female=252}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f251.png", "f252.png"}},
			{name = "Nightmare Addon", thingId = {male=268,female=269}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 12, icons = {"f268.png", "f269.png"}},
			{name = "Jester Addon", thingId = {male=273,female=270}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 10, icons = {"f273.png", "f270.png"}},
			{name = "Brotherhood Addon", thingId = {male=279,female=278}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 10, icons = {"f279.png", "f278.png"}},
			{name = "Afflicted Addon", thingId = {male=430,female=431}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 7, icons = {"f430.png", "f431.png"}},
			{name = "Summoner Addon", thingId = {male=133,female=141}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 60, icons = {"f133.png", "f141.png"}},
			
			{name = "Assassin Addon", thingId = {male=152,female=156}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 18, icons = {"f152.png", "f156.png"}},
			
			 {name = "Mage Addon", thingId = {male=130,female=138}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 150, icons = {"f130.png", "f138.png"}},
			 
			 {name = "Barbarian Addon", thingId = {male=143,female=147}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 18, icons = {"f143.png", "f147.png"}},
			
			 {name = "Oriental Addon", thingId = {male=146,female=150}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f146.png", "f150.png"}},
			
			{name = "Pirate Addon", thingId = {male=151,female=155}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 23, icons = {"f151.png", "f155.png"}},
			
			{name = "Wayfarer Addon", thingId = {male=367,female=366}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 20, icons = {"f367.png", "f366.png"}},
			
			{name = "Elementalist Addon", thingId = {male=432,female=433}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 20, icons = {"f432.png", "f433.png"}},
			 {name = "Entrepreneur Addon", thingId = {male=472,female=471}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f472.png", "f471.png"}},
			{name = "Crystal Warlord Addon", thingId = {male=512,female=513}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f512.png", "f513.png"}},
			
			{name = "Soil Guardian Addon", thingId = {male=516,female=514}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f516.png", "f514.png"}},
			
			{name = "Cave Explorer Addon", thingId = {male=544,female=575}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f544.png", "f575.png"}},
			
			{name = "Dream Warden Addon", thingId = {male=577,female=578}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f577.png", "f578.png"}},
			
			{name = "Glooth Engineer Addon", thingId = {male=610,female=618}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f610.png", "f618.png"}},
			
			 {name = "Jersey", thingId = {male=619,female=620}, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT, price = 15, icons = {"f619.png", "f620.png"}},
			
			 {name = "Champion Addon", thingId = {male=633,female=632}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f633.png", "f632.png"}},
			
			 {name = "Conjurer Addon", thingId = {male=634,female=635}, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT, price = 15, icons = {"f634.png", "f635.png"}},
			
			 {name = "Beastmaster Addon", thingId = {male=637,female=636}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f637.png", "f636.png"}},
			
			 {name = "Chaos Acolyte Addon", thingId = {male=665,female=664}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f665.png", "f664.png"}},
			
			 {name = "Death Herald Addon", thingId = {male=667,female=666}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f667.png", "f666.png"}},
			
			 {name = "Ranger Addon", thingId = {male=684,female=683}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f684.png", "f683.png"}},
			
			 {name = "Ceremonial Garb Addon", thingId = {male=695,female=694}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f695.png", "f694.png"}},
			
			 {name = "Puppeteer Addon", thingId = {male=697,female=696}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 20, icons = {"f697.png", "f696.png"}},
			
			 {name = "Spirit Caller Addon", thingId = {male=699,female=698}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f699.png", "f698.png"}},
			
			 {name = "Evoker Addon", thingId = {male=725,female=724}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f725.png", "f724.png"}},
			
			 {name = "Seaweaver Addon", thingId = {male=733,female=732}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f733.png", "f732.png"}},
			
			 {name = "Recruiter Addon", thingId = {male=746,female=745}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f746.png", "f745.png"}},
			
			 {name = "Sea Dog Addon", thingId = {male=750,female=749}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f750.png", "f749.png"}},
			
			 {name = "Royal Pumpkin Addon", thingId = {male=760,female=759}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f760.png", "f759.png"}},
			
			{name = "Rift Warrior Addon", thingId = {male=846,female=845}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f846.png", "f845.png"}},
			
			 {name = "Winter Warden Addon", thingId = {male=853,female=852}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f853.png", "f852.png"}},
			
			 {name = "Philosopher Addon", thingId = {male=874,female=873}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f874.png", "f873.png"}},
			
			 {name = "Arena Champion Addon", thingId = {male=884,female=885}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 15, icons = {"f884.png", "f885.png"}},
		
		    {name = "Herbalist Addon", thingId = {male=1021,female=1020}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1021.png", "f1020.png"}},
			
			 {name = "Sun Priest Addon", thingId = {male=1023,female=1024}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1023.png", "f1024.png"}},
			 
			 {name = "Makeshift Warrior Addon", thingId = {male=1042,female=1043}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1042.png", "f1043.png"}},
			
			 {name = "Siege Master Addon", thingId = {male=1051,female=1050}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1051.png", "f1050.png"}},
			
			{name = "Mercenary Addon", thingId = {male=1056,female=1057}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1056.png", "f1057.png"}},
		    
			{name = "Battle Mage Addon", thingId = {male=1069,female=1070}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1069.png", "f1070.png"}},
			
			 {name = "Discoverer Addon", thingId = {male=1094,female=1095}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1094.png", "f1095.png"}},
			
			{name = "Sinister Archer Addon", thingId = {male=1102,female=1103}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1102.png", "f1103.png"}},
		
			 {name = "Pumpkin Mummy Addon", thingId = {male=1127,female=1128}, addon = 3, type = GameStore.OfferTypes.OFFER_TYPE_OUTFIT_ADDON, price = 25, icons = {"f1127.png", "f1128.png"}},
		
		}
	},
	
	{
		name = "Weapons",
		state = GameStore.States.STATE_NONE,
		icons = {"Category_ExtraServices.png"},
		rookgaard = true,
		offers = {
		    {name = "Shiny Blade", thingId = 18465, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"18465.png"}, description = "(Atk: 50, Def: 35 +3, Sword Fighting +1). It can only be wielded properly by players of level 8 or higher. It weighs 45.00 oz. Imbuement Slots: 1"},
			{name = "Crystalline Axe", thingId = 18451, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"18451.png"}, description = "(Atk: 51, Def: 29 +3, Axe Fighting +1). It can only be wielded properly by players of level 8 or higher. It weighs 76.00 oz. Imbuement Slots: 1"},
			{name = "Mycological Mace", thingId = 18452, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"18452.png"}, description = "(Atk: 50, Def: 31 +3, Club Fighting +1). It can only be wielded properly by players of level 8 or higher. It weighs 59.00 oz. Imbuement Slots: 1"},
			{name = "Rift Bow", thingId = 25522, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"Rift_Bow.png"},description = "(Range: 7, Atk +5, Hit% +3). It can only be wielded properly by Paladins of level 8 or higher. It weighs 41.00 oz. Imbuement Slots: 3"},
		    {name = "Thorn Spitter", thingId = 16111, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"16111.png"}, description = "(Range: 6, Atk +9, Hit% +1). It can only be wielded properly by players of level 8 or higher. It weighs 126.00 oz. Imbuement Slots: 3"},
			{name = "The Devileye", thingId = 8852, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 13, icons = {"8852.png"}, description = "(Range: 6, Atk +20, Hit% -20). It can only be wielded properly by Paladins of level 8 or higher. It weighs 55.00 oz. Imbuement Slots: 3"},
			{name = "Umbral Axe", thingId = 22405, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22405.png"}, description = "(Atk: 51, Def: 27 +2). It can only be wielded properly by Knights of level 8 or higher. It weighs 85.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Chopper", thingId = 22408, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22408.png"}, description = "Atk: 52, Def: 30). It can only be wielded properly by Knights of level 8 or higher. It weighs 115.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Blade", thingId = 22399, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22399.png"}, description = "(Atk: 50, Def: 29 +2). It can only be wielded properly by Knights of level 8 or higher. It weighs 59.00 oz. Imbuement Slots: 1"},
		    {name = "Umbral Slayer", thingId = 22402, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22402.png"}, description = "(Atk: 52, Def: 31). It can only be wielded properly by Knights of level 8 or higher. It weighs 95.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Mace", thingId = 22411, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22411.png"}, description = "(Atk: 50, Def: 26 +2). It can only be wielded properly by Knights of level 8 or higher. It weighs 85.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Hammer", thingId = 22414, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22414.png"}, description = "(Atk: 53, Def: 30). It can only be wielded properly by Knights of level 8 or higher. It weighs 165.00 oz. Imbuement Slots: 1"},
		    {name = "Umbral Bow", thingId = 22417, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22417.png"}, description = "(Range: 7, Atk +4, Hit% +5). It can only be wielded properly by Paladins of level 8 or higher. It weighs 45.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Crossbow", thingId = 22420, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22420.png"}, description = "(Range: 5, Atk +6, Hit% +2). It can only be wielded properly by Paladins of level 8 or higher. It weighs 125.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Spellbook", thingId = 22423, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 12, icons = {"22423.png"}, description = "(Def: 16, Magic Level +2, protection Earth +3%, Energy +3%, Fire +3%, Ice +3%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 35.00 oz. Imbuement Slots: 1"},	    
			{name = "Umbral Masterblade", thingId = 22400, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22400.png"}, description = "(Atk:52, Def:31 +3, sword fighting +1). It can only be wielded properly by Knights of level 8 or higher. It weighsIt weighs 55.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Master Slayer", thingId = 22403, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22403.png"}, description = "(Atk: 54, Def: 35, Sword Fighting +3). It can only be wielded properly by Knights of level 8 or higher. It weighs 90.00 oz. Imbuement Slots: 2"},
			{name = "Umbral Master Axe", thingId = 22406, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22406.png"}, description = "(Atk: 53, Def: 30 +3, Axe Fighting +1). It can only be wielded properly by Knights of level 8 or higher. It weighs 80.00 oz. Imbuement Slots: 1"},
		    {name = "Umbral Master Chopper", thingId = 22409, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22409.png"}, description = "(Atk: 54, Def: 34, Axe Fighting +3). It can only be wielded properly by Knights of level 8 or higher. It weighs 110.00 oz. Imbuement Slots: 2"},
			{name = "Umbral Master Mace", thingId = 22412, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22412.png"}, description = "(Atk: 52, Def: 30 +3, Club Fighting +1). It can only be wielded properly by Knights of level 8 or higher. It weighs 80.00 oz. Imbuement Slots: 1"},
			{name = "Umbral Master Hammer", thingId = 22415, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22415.png"}, description = "(Atk: 55, Def: 34, Club Fighting +3). It can only be wielded properly by Knights of level 8 or higher. It weighs 160.00 oz. Imbuement Slots: 2"},
		    {name = "Umbral Master Bow", thingId = 22418, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22418.png"}, description = "(Range: 7, Atk +6, Hit% +5, Distance Fighting +3). It can only be wielded properly by Paladins of level 8 or higher. It weighs 40.00 oz. Imbuement Slots: 2"},
			{name = "Umbral Master Crossbow", thingId = 22421, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22421.png"}, description = "(Range: 7, Atk +6, Hit% +5, Distance Fighting +3). It can only be wielded properly by Paladins of level 8 or higher. It weighs 40.00 oz. Imbuement Slots: 2"},
			{name = "Umbral Master Spellbook", thingId = 22424, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 20, icons = {"22424.png"}, description = "(Def: 20, Magic Level +4, protection Earth +5%, Energy +5%, Fire +5%, Ice +5%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 30.00 oz. Imbuement Slots: 1"},	    
			{name = "Falcon Rod", thingId = 32416, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Rod.png"}, description = "(Magic Level +3, Energy +8%). It can only be wielded properly by Druids of level 8 or higher. It weighs 37.00 oz. Imbuement Slots: 2"},
			{name = "Falcon Wand", thingId = 32417, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Wand.png"}, description = "(Magic Level +3, Fire +8%). It can only be wielded properly by Sorcerer of level 8 or higher. It weighs 33.00 oz. Imbuement Slots: 2"},
			{name = "Falcon Bow", thingId = 32418, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Bow.png"}, description = "(Range: 6, Atk +7, Hit% +5,Distance Fighting +2, protection Fire +5%). It can only be wielded properly by players of level 8 or higher. It weighs 35.00 oz. Imbuement Slots: 3"},
			{name = "Falcon Longsword", thingId = 32423, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Longsword.png"}, description = "(Atk: 54, Def: 34, Sword Fighting +4, protection Earth +10%). It can only be wielded properly by Knights of level 8 or higher. It weighs 82.00 oz. Imbuement Slots: 2"},
			{name = "Falcon Battleaxe", thingId = 32424, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Battleaxe.png"},description = "(Atk: 10 + 45 Energy, Def: 33, Axe Fighting +4, protection Energy +12%). It can only be wielded properly by Knights of level 8 or higher. It weighs 95.00 oz. Imbuement Slots: 2"},
			{name = "Falcon Mace", thingId = 32425, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Mace.png"}, description = "(Atk: 11 + 41 Energy, Def: 33 +3, Club Fighting +3, protection Energy +7%). It can only be wielded properly by Knights of level 8 or higher. It weighs 68.00 oz. Imbuement Slots: 2"},
			{name = "Blade of Destruction", thingId = 30684, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Blade_of_Destruction.png"}, description = "(Atk: 50, Def: 33). It can only be wielded properly by Knights of level 8 or higher. It weighs 50.00 oz. Imbuement Slots: 3"},
			{name = "Slayer of Destruction", thingId = 30685, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Slayer_of_Destruction.png"}, description = "(Atk: 52, Def: 32). It can only be wielded properly by Knights of level 8 or higher. It weighs 70.00 oz. Imbuement Slots: 3"},
			{name = "Axe of Destruction", thingId = 30686, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Axe_of_Destruction.png"}, description = "(Atk: 51, Def: 31). It can only be wielded properly by Knights of level 8 or higher. It weighs 50.00 oz. Imbuement Slots: 3"},
			{name = "Chopper of Destruction", thingId = 30687, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Chopper_of_Destruction.png"}, description = "(Atk: 52, Def: 32). It can only be wielded properly by Knights of level 8 or higher. It weighs 70.00 oz. Imbuement Slots: 3"},      
			{name = "Mace of Destruction", thingId = 30688, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Mace_of_Destruction.png"}, description = "(Atk: 50, Def: 30). It can only be wielded properly by Knights of level 8 or higher. It weighs 50.00 oz. Imbuement Slots: 3"},
			{name = "Hammer of Destruction", thingId = 30689, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Hammer_of_Destruction.png"},description = "(Atk: 53, Def: 30). It can only be wielded properly by Knights of level 8 or higher. It weighs 70.00 oz. Imbuement Slots: 3"},
			{name = "Bow of Destruction", thingId = 30690, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Bow_of_Destruction.png"}, description = "(Range: 6, Atk +6, Hit% +6). It can only be wielded properly by Paladins of level 8 or higher. It weighs 65.00 oz. Imbuement Slots: 3"},
			{name = "Crossbow of Destruction", thingId = 30691, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Crossbow_of_Destruction.png"}, description = "(Range: 5, Atk +6, Hit% +6). It can only be wielded properly by Paladins of level 8 or higher. It weighs 65.00 oz. Imbuement Slots: 3"},
			{name = "Wand of Destruction", thingId = 30692, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Wand_of_Destruction.png"}, description = "(Magic Level +2). It can only be wielded properly by Sorcerers of level 8 or higher. It weighs 35.00 oz. Imbuement Slots: 2"},
			{name = "Rod of Destruction", thingId = 30693, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"Rod_of_Destruction.png"}, description = "(Magic Level +2). It can only be wielded properly by Druids of level 8 or higher. It weighs 35.00 oz. Imbuement Slots: 2"},

			}
	},
	
	{
		name = "Helmets, Armors, Legs, Shields and Boots",
		state = GameStore.States.STATE_NONE,
		icons = {"Category_ExtraServices.png"},
		rookgaard = true,
		offers = {
			{name = "Depth Galea", thingId = 15651, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 8, icons = {"15651.png"}, description = "(Arm: 8, Speed +300 *, protection Drowning +100%). It can only be wielded properly by players of level 8 or higher. It weighs 46.00 oz. Enables underwater exploration."},
			{name = "Falcon Coif", thingId = 32415, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Coif.png"}, description = "(Arm:10, distance fighting +2, shielding +2, protection physical +3%, fire +10%). It can only be wielded properly by paladins and knights of level 8 or higher. It weighs 28.00 oz. Imbuement Slots: 2"},			
			{name = "Falcon circlet", thingId = 32414, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Circlet.png"}, description = "(Arm: 8, magic level +2, protection fire +9%). It can only be wielded properly by sorcerers and druids of level 8 or higher. It weighs 43.00 oz. Imbuement Slots: 1"},
			{name = "Gnome helmet", thingId = 30882, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"Gnome_Helmet.png"}, description = "(Arm: 7, magic level +2, protection physical +3%, energy +8%, ice -2%). It can only be wielded properly by sorcerers and druids of level 8 or higher. It weighs 43.00 oz. Imbuement Slots: 1"},
			{name = "Elite Draken Helmet", thingId = 12645, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"12645.png"}, description = "(Arm: 9, Distance Fighting +1, protection Death +3%). It can only be wielded properly by Paladins of level 8 or higher. It weighs 43.00 oz. Imbuement Slots: 1"},
			{name = "Gill Gugel", thingId = 18398, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 8, icons = {"18398.png"}, description = "(Arm: 5, Magic Level +2, protection Earth +6%, Fire -6%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 9.00 oz."},
			{name = "Golden Helmet", thingId = 2471, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 49, icons = {"2471.png"}, description = "(Arm: 12). It weighs 32.00 oz. Imbuement Slots: 2. It is the famous Helmet of the Stars."},
			{name = "Prismatic Helmet", thingId = 18403, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"18403.png"}, description = "(Arm: 9, Shielding +1, protection Physical +5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 56.00 oz."},
			{name = "Werewolf Helmet", thingId = 24718, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"Werewolf_Helmet.png"}, description = "(Arm: 9, Speed +15). It can only be wielded properly by players of level 8 or higher. It weighs 25.00 oz."},
		    {name = "Yalahari Mask", thingId = 9778, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 8, icons = {"9778.png"}, description = "(Arm: 5, Magic Level +2). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 35.00 oz. Imbuement Slots: 1"},
			{name = "Falcon Plate", thingId = 32419, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Plate.png"}, description = "(Arm: 18, Shielding +4, protection Physical +12%). It can only be wielded properly by Knights of level 8 or higher. It weighs 188.00 oz. Imbuement Slots: 2"},
			{name = "Demon Armor", thingId = 2494, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"2494.png"}, description = "(Arm: 16). It weighs 80.00 oz. Imbuement Slots: 2"},
			{name = "Earthborn Titan Armor", thingId = 8882, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"8882.png"}, description = "(Arm: 15, Axe Fighting +2, protection Earth +5%, Fire -5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 120.00 oz."},
			{name = "Windborn Colossus Armor", thingId = 8883, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"8883.png"}, description = "(Arm: 15, Club Fighting +2, protection Energy +5%, Earth -5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 120.00 oz."},
			{name = "Fireborn Giant Armor", thingId = 8881, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"8881.png"}, description = "(Arm: 15, Sword Fighting +2, protection Fire +5%, Ice -5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 120.00 oz."},
			{name = "Master Archers Armor", thingId = 8888, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"8888.png"}, description = "(Arm: 15, Distance Fighting +3). It can only be wielded properly by Paladins of level 8 or higher. It weighs 69.00 oz. Imbuement Slots: 1"},
			{name = "Royal Draken Mail", thingId = 12642, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"12642.png"}, description = "(Arm: 16, Shielding +3, protection Physical +5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 130.00 oz."},
		    {name = "Gill Coat", thingId = 18399, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 8, icons = {"18399.png"}, description = "(Arm: 12, Magic Level +1, protection Earth +10%, Fire -10%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 19.00 oz."},
			{name = "Royal Scale Robe", thingId = 12643, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"12643.png"}, description = "(Arm: 12, Magic Level +2, protection Fire +5%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 45.00 oz."},
			{name = "Prismatic Armor", thingId = 18404, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"18404.png"}, description = "(Arm: 15, Speed +15, protection Physical +5%). It can only be wielded properly by Knights and Paladins of level 8 or higher. It weighs 79.00 oz. Imbuement Slots: 1"},
			{name = "Depth Lorica", thingId = 15407, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"15407.png"}, description = "(Arm: 16, Distance Fighting +3, protection Death +5%). It can only be wielded properly by Paladins of level 8 or higher. It weighs 145.00 oz. Imbuement Slots: 2"},
			{name = "Furious Frock", thingId = 21725, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 16, icons = {"21725.png"}, description = "(Arm: 12, Magic Level +2, protection Fire +5%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 34.00 oz."},
		    {name = "Earthmind raiment", thingId = 25191, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"Earthmind_Raiment.png"}, description = "(Arm: 15, magic level +4, protection earth +8%, fire -8% It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 34.00 oz."},
		    {name = "Firemind raiment", thingId = 25190, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"Firemind_Raiment.png"}, description = "(Arm: 15, magic level +4, protection fire +8%, ice -8%. It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 34.00 oz."},
		    {name = "Frostmind raiment", thingId = 25193, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"Frostmind_Raiment.png"}, description = "(Arm: 15, magic level +4, protection ice +8%, energy -8%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 120.00 oz. Imbuement Slots: 2"},
		    {name = "Thundermind raiment", thingId = 25192, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"Thundermind_Raiment.png"}, description = "(Arm: 15, magic level +4, protection energy +8%, earth -8%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 34.00 oz."},
			{name = "Gnome Armor", thingId = 30883, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"Gnome_Armor.png"}, description = "(Arm:17, distance fighting +3, protection physical +4%, energy +8%, ice -2%). It can only be wielded properly by Paladins of level 8 or higher. It weighs 34.00 oz."},
			{name = "Ornate Chestplate", thingId = 15406, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"15406.png"}, description = "(Arm: 16, Shielding +3, protection Physical +8%). It can only be wielded properly by Knights of level 8 or higher. It weighs 156.00 oz. Imbuement Slots: 2"},
			{name = "Falcon Greaves", thingId = 32420, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Greaves.png"}, description = "(Arm: 10, Distance Fighting +3, Sword Fighting +3, Club Fighting +3, Axe Fighting +3, protection Physical +7%, Ice +7%). It can only be wielded properly by Knights and Paladins of level 8 or higher. It weighs 36.00 oz."},
			{name = "Demon Legs", thingId = 2495, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"2495.png"}, description = "(Arm: 9). It weighs 70.00 oz."},
			{name = "Grasshopper Legs", thingId = 15490, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"15490.png"}, description = "(Arm: 7, Speed +10). It can only be wielded properly by players of level 8 or higher. It weighs 32.00 oz."},
			{name = "Depth Ocrea", thingId = 15409, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"15409.png"}, description = "(Arm: 8, protection Mana Drain +15%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 48.00 oz."},
			{name = "Prismatic Legs", thingId = 18405, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"18405.png"}, description = "(Arm: 8, Distance Fighting +2, protection Physical +3%). It can only be wielded properly by Paladins of level 8 or higher. It weighs 71.00 oz."},
			{name = "Gill Legs", thingId = 18400, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 8, icons = {"18400.png"}, description = "(Arm: 7, Magic Level +1, protection Earth +8%, Fire -8%). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 28.00 oz."},
			{name = "Ornate Legs", thingId = 15412, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"15412.png"}, description = "(Arm: 8, protection Physical +5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 77.00 oz."},
		    {name = "Dwarven Legs", thingId = 2504, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 14, icons = {"2504.png"}, description = "(Arm: 7, protection Physical +3%). It weighs 49.00 oz."},
            {name = "Gnome Legs", thingId = 30884, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"Gnome_Legs.png"}, description = "(Arm:9, magic level +2, protection energy +7%, ice -2%). It can only be wielded properly by druids and sorcerers of level 8 or higher. It weighs 49.00 oz."},
			{name = "Icy Culottes", thingId = 21700, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"21700.png"}, description = "(Arm: 8, protection Ice +8%). It can only be wielded properly by Sorcerers and Druids. It weighs 15.00 oz."},
			{name = "Depth Scutum", thingId = 15411, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"15411.png"}, description = "(Def: 25, Magic Level +2). It can only be wielded properly by Sorcerers and Druids of level 8 or higher. It weighs 30.00 oz. Imbuement Slots: 1"},
			{name = "Falcon Escutcheon", thingId = 32422, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 29, icons = {"Falcon_Escutcheon.png"}, description = "(Def: 40, protection Physical + 7%, Fire +15%). It can only be wielded properly by Knights and Paladins of level 8 or higher. It weighs 60.00 oz. Imbuement Slots: 1"},
			{name = "Falcon Shield", thingId = 32421, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 24, icons = {"Falcon_Shield.png"}, description = "(Def: 39, protection Physical +6%, Fire +10%). It can only be wielded properly by Knights and Paladins of level 8 or higher. It weighs 57.00 oz. Imbuement Slots: 1"},
			{name = "Shield of Corruption", thingId = 12644, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"12644.png"}, description = "(Def: 36, Sword Fighting +3). It can only be wielded properly by Knights of level 8 or higher. It weighs 49.00 oz. Imbuement Slots: 1"},
			{name = "Ornate Shield", thingId = 15413, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 9, icons = {"15413.png"}, description = "(Def: 36, protection Physical +5%). It can only be wielded properly by Knights of level 8 or higher. It weighs 71.00 oz. Imbuement Slots: 1"},
			{name = "Prismatic Shield", thingId = 18410, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 11, icons = {"18410.png"}, description = "(Def: 37, Shielding +2, protection Physical +4%). It can only be wielded properly by Knights of level 8 or higher. It weighs 72.00 oz."},
			{name = "Gnome Shield", thingId = 30885, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 19, icons = {"Gnome_Shield.png"}, description = "(Def:38, shielding +2, protection physical +6%, energy +8%, ice -2%). It can only be wielded properly by paladins and knights of level 8 or higher. It weighs 60.00 oz. Imbuement Slots: 1"},
			{name = "Golden Boots", thingId = 2646, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_ITEM, price = 18, icons = {"2646.png"}, description = "(Arm: 4). It weighs 31.00 oz. Imbuement Slots: 1"},
			
			}
	},
	
	{
		name = "Potions & Runes",
		state = GameStore.States.STATE_NONE,
		description = "Buy potions to refil your character's hit points and mana.",
		rookgaard = true,
		icons = {"Category_Potions.png"},
		offers = {
			{name = "Health Potion", thingId = 7618, count = 125, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Product_Potion_HealthPotion.png"}},
			{name = "Health Potion", thingId = 7618, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Product_Potion_HealthPotion.png"}},
			{name = "Strong Health Potion", thingId = 7588, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Product_Potion_StrongHealthPotion.png"}},
			{name = "Strong Health Potion", thingId = 7588, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Product_Potion_StrongHealthPotion.png"}},
			{name = "Great Health Potion", thingId = 7591, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Malvera_Product_Potion_Great_healt_potion.png"}},
			{name = "Great Health Potion", thingId = 7591, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Malvera_Product_Potion_Great_healt_potion.png"}},
			{name = "Ultimate Health Potion", thingId = 8473, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Malvera_Product_Potion_ultimate_health_potion.png"}},
			{name = "Ultimate Health Potion", thingId = 8473, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Malvera_Product_Potion_ultimate_health_potion.png"}},
			{name = "Supreme Health Potion", thingId = 26031, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Malvera_Product_Potion_Ultimate_mana_potion.png"}},
			{name = "Supreme Health Potion", thingId = 26031, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Malvera_Product_Potion_Ultimate_mana_potion.png"}},
			{name = "Mana Potion", thingId = 7620, count = 125, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Product_Potion_ManaPotion.png"}},
			{name = "Mana Potion", thingId = 7620, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Product_Potion_ManaPotion.png"}},
			{name = "Strong Mana Potion", thingId = 7589, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Product_Potion_StrongManaPotion.png"}},
			{name = "Strong Mana Potion", thingId = 7589, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Product_Potion_StrongManaPotion.png"}},
			{name = "Great Mana Potion", thingId = 7590, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Malvera_Product_Potion_Supreme_healt_potion.png"}},
			{name = "Great Mana Potion", thingId = 7590, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Malvera_Product_Potion_Supreme_healt_potion.png"}},
			{name = "Ultimate Mana Potion", thingId = 26029, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Malvera_Product_Potion_Ultimate_mana_potion.png"}},
			{name = "Ultimate Mana Potion", thingId = 26029, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Malvera_Product_Potion_Ultimate_mana_potion.png"}},		
			{name = "Great Spirit Potion", thingId = 8472, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Product_Potion_GreatSpiritPotion.png"}},
			{name = "Great Spirit Potion", thingId = 8472, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Product_Potion_GreatSpiritPotion.png"}},
			{name = "Ultimate Spirit Potion", thingId = 26030, count = 100, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 2, icons = {"Product_Potion_UltimateSpiritPotion.png"}},
			{name = "Ultimate Spirit Potion", thingId = 26030, count = 300, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, price = 4, icons = {"Product_Potion_UltimateSpiritPotion.png"}},	
			{name = "Animate Dead Rune", description = "Here you can purchase a stack of '250 Animate Dead Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2316, count = 250, icons = {"Product_Rune_AnimateDeadRune.png"}},
			{name = "Avalanche Rune", description = "Here you can purchase a stack of '250 Avalanche Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2274, count = 250, icons = {"Product_Rune_AvalancheRune.png"}},
			{name = "Chameleon Rune", description = "Here you can purchase a stack of '250 Chameleon Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2291, count = 250, icons = {"Product_Rune_ChameleonRune.png"}},
			{name = "Convince Creature Rune", description = "Here you can purchase a stack of '250 Convince Creature Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2290, count = 250, icons = {"Product_Rune_ConvinceRune.png"}},
			{name = "Cure Poison Rune", description = "Here you can purchase a stack of '250 Cure Poison Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2266, count = 250, icons = {"Product_Rune_CurePoisonRune.png"}},
			{name = "Disintegrate Rune", description = "Here you can purchase a stack of '250 Disintegrate Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2310, count = 250, icons = {"Product_Rune_DisintegrateRune.png"}},
			{name = "Energy Bomb Rune", description = "Here you can purchase a stack of '250 Energy Bomb Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2262, count = 250, icons = {"Product_Rune_EnergyBombRune.png"}},
			{name = "Energy Field Rune", description = "Here you can purchase a stack of '250 Energy Field Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2277, count = 250, icons = {"Product_Rune_EnergyFieldRune.png"}},
			{name = "Energy Wall Rune", description = "Here you can purchase a stack of '250 Energy Wall Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2279, count = 250, icons = {"Product_Rune_EnergyWallRune.png"}},
			{name = "Explosion Rune", description = "Here you can purchase a stack of '250 Explosion Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2313, count = 250, icons = {"Product_Rune_ExplosionRune.png"}},
			{name = "Fireball Rune", description = "Here you can purchase a stack of '250 Fireball Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2302, count = 250, icons = {"Product_Rune_FireballRune.png"}},
			{name = "Fire Bomb Rune", description = "Here you can purchase a stack of '250 Fire Bomb Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2305, count = 250, icons = {"Product_Rune_FireBombRune.png"}},
			{name = "Fire Field Rune", description = "Here you can purchase a stack of '250 Fire Field Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2301, count = 250, icons = {"Product_Rune_FireFieldRune.png"}},
			{name = "Fire Wall Rune", description = "Here you can purchase a stack of '250 Fire Wall Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2303, count = 250, icons = {"Product_Rune_FireWallRune.png"}},
			{name = "Great Fireball Rune", description = "Here you can purchase a stack of 'Great Fireball Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2304, count = 250, icons = {"Product_Rune_GreatFireBallRune.png"}},
			{name = "Icicle Rune", description = "Here you can purchase a stack of '250 Icicle Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2271, count = 250, icons = {"Product_Rune_IcicleRune.png"}},
			{name = "Intense Healing Rune", description = "Here you can purchase a stack of '250 Intense Healing Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2265, count = 250, icons = {"Product_Rune_IntenseHealingRune.png"}},
			{name = "Magic Wall Rune", description = "Here you can purchase a stack of '250 Magic Wall Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2293, count = 250, icons = {"Product_Rune_MagicWallRune.png"}},
			{name = "Paralyse Rune", description = "Here you can purchase a stack of '250 Paralyse Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2278, count = 250, icons = {"Product_Rune_ParalyseRune.png"}},
			{name = "Poison Bomb Rune", description = "Here you can purchase a stack of '250 Poison Bomb Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2286, count = 250, icons = {"Product_Rune_PoisonBombRune.png"}},
			{name = "Poison Wall Rune", description = "Here you can purchase a stack of '250 Poison Wall Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2289, count = 250, icons = {"Product_Rune_PoisonWallRune.png"}},
			{name = "Soulfire Rune", description = "Here you can purchase a stack of '250 Soulfire Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2308, count = 250, icons = {"Product_Rune_SoulfireRune.png"}},
			{name = "Stone Shower Rune", description = "Here you can purchase a stack of '250 Stone Shower Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2288, count = 250, icons = {"Product_Rune_StoneShowerRune.png"}},
			{name = "Sudden Death Rune", description = "Here you can purchase a stack of '250 Sudden Death Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2268, count = 250, icons = {"Product_Rune_SuddenDeathRune.png"}},
			{name = "Thunderstorm Rune", description = "Here you can purchase a stack of '250 Thunderstorm Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2315, count = 250, icons = {"Product_Rune_ThunderstormRune.png"}},
			{name = "Ultimate Healing Rune", description = "Here you can purchase a stack of '250 Ultimate Healing Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2273, count = 250, icons = {"Product_Rune_UltimateHealingRune.png"}},
			{name = "Wild Growth Rune", description = "Here you can purchase a stack of '250 Wild Growth Runes'. Use them to unleash their magical energy when in need of it.Note, characters with a protection zone block or a battle sign cannot purchase runes. Also, characters cannot buy runes that exceed their capacity. Finally, runes bought in the Store can only be used by the character that makes the purchase. For this reason, the purchased runes need to fit the character's level and magic level.", price = 3, type = GameStore.OfferTypes.OFFER_TYPE_STACKABLE, thingId = 2269, count = 250, icons = {"Product_Rune_WildGrowthRune.png"}},
		 
		 }
	},
	
	{
		name = "Decorations House",
		state = GameStore.States.STATE_NONE,
		description = "Buy exceptional equipment to upgrade your Tibia House",
		icons = {"Category_HouseEquipment.png"},
		rookgaard = true,
		offers = {
				-- {name = "Alchemistic Cabinet", thingId = 31192, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 100, icons = {"Product_HouseEquipment_alchemisticcabinet.png"}},
				-- {name = "Alchemistic Bookstand", thingId = 31211, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 100, icons = {"Product_HouseEquipment_alchemisticbookstand.png"}},
				-- {name = "Alchemistic Chair", thingId = 31258, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 50, icons = {"Product_HouseEquipment_alchemisticchair.png"}},
				-- {name = "Alchemistic Scales", thingId = 31215, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 120, icons = {"Product_HouseEquipment_alchemisticscales.png"}},
				-- {name = "Alchemist Table", thingId = 31194, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 80, icons = {"Product_HouseEquipment_alchemistictable.png"}},
				-- {name = "Pile of Alchemist Books", thingId = 31219, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 120, icons = {"Product_HouseEquipment_pileofalchemisticbooks.png"}},
				-- {name = "Alchemist Cup Board", thingId = 31221, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 50, icons = {"Product_HouseEquipment_alchemisticcupboard.png"}},
				-- {name = "Light of Change", thingId = 31201, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 120, icons = {"Product_HouseEquipment_lightofchange.png"}},
				-- {name = "Torch of Change", thingId = 31207, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 120, icons = {"Product_HouseEquipment_torchofchange.png"}},
				-- {name = "Ferumbras Bust", thingId = 31305, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 70, icons = {"Product_HouseEquipment_ferumbrasbust.png"}},
				-- {name = "Queen Eloise Bust", thingId = 31307, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 50, icons = {"Product_HouseEquipment_queeneloisebust.png"}},
				-- {name = "King Tibianus Bust", thingId = 31309, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 50, icons = {"Product_HouseEquipment_kingtibianusbust.png"}},
				-- {name = "Arrival At Thais Painting", thingId = 31226, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 50, icons = {"Product_HouseEquipment_arrivalatthaispainting.png"}},
				-- {name = "Tibia Street Painting", thingId = 31228, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 100, icons = {"Product_HouseEquipment_tibiastreetspainting.png"}},
				-- {name = "Ferumbras Portrait", thingId = 31230, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 100, icons = {"Product_HouseEquipment_ferumbrasportrait.png"}},
				{name = "Wheat Carpet", thingId = 29386, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Product_HouseEquipment_Carpet23.png"}},
				{name = "Crested Carpet", thingId = 29388, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Product_HouseEquipment_Carpet24.png"}},
				{name = "Decorated Carpet", thingId = 29390, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Product_HouseEquipment_Carpet25.png"}},
				{name = "Ornate Table", thingId = 29396, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_EgyptianFurniture_Table.png"}},
				{name = "Ornate Chair", thingId = 29394, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_EgyptianFurniture_Chair.png"}},
				{name = "Ornate Chest", thingId = 29401, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_EgyptianFurniture_Chest.png"}},
				{name = "Ornate Cabinet", thingId = 29398, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_EgyptianFurniture_Cupboard.png"}},
				{name = "Terrarium Snake", thingId = 29405, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 8, icons = {"Product_HouseEquipment_Housepet_Snake.png"}},
				{name = "Demon Pet", thingId = 29409, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 10, icons = {"Product_HouseEquipment_Housepet_LilDemon.png"}},
				{name = "Verdant Chair", thingId = 29336, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_NaturalFurniture_Chair.png"}},
				{name = "Verdant Cabinet", thingId = 29340, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_NaturalFurniture_Cabinet.png"}},
				{name = "Verdant Trunk", thingId = 29346, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_NaturalFurniture_Chest.png"}},
				{name = "Verdant Table", thingId = 29347, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_NaturalFurniture_Table.png"}},
				{name = "Venoream Table Clock", thingId = 29347, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_FancyClock.png"}},
				{name = "Verdant Carpet", thingId = 29349, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Product_HouseEquipment_Carpet_18.png"}},
				{name = "Shaggy Carpet", thingId = 29351, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Product_HouseEquipment_Carpet_19.png"}},
				{name = "Mystic Carpet", thingId = 29353, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Product_HouseEquipment_Carpet_20.png"}},
				{name = "Wooden Planks", thingId = 29358, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_Carpet_22.png"}},
				{name = "Stone Tiles", thingId = 29355, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_Carpet_21.png"}},
				{name = "Hrodmiran Weapons Rack", thingId = 29316, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Product_HouseEquipment_HrodmirianWeaponRack.png"}},
				{name = "Bath Tube", thingId = 29311, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 5, icons = {"Product_HouseEquipment_Bathtub.png"}},
				{name = "Terrarium Spider", thingId = 29313, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 8, icons = {"Product_HouseEquipment_BabyGiantSpider.png"}},
				{name = "Daily Reward Shrine", thingId = 29020, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 5, icons = {"Product_HouseEquipment_DailyRewardShrine.png"}},
				{name = "Shiny Daily Reward Shrine", thingId = 29023, count = 1, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 5, icons = {"Product_HouseEquipment_ShinyDailyRewardShrine.png"}},
				{name = "Health Cask", thingId = 28555, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Health_Cask.png"}},
				{name = "Strong Health Cask", thingId = 28556, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Strong_Health_Cask.png"}},
				{name = "Great Health Cask", thingId = 28557, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 4, icons = {"Great_Health_Cask.png"}},
				{name = "Ultimate Health Cask", thingId = 28558, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 5, icons = {"Ultimate_Health_Cask.png"}},
				{name = "Supreme Health Cask", thingId = 28559, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 47, icons = {"Supreme_Health_Cask.png"}},
                {name = "Mana Cask", thingId = 28565, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 2, icons = {"Mana_Cask.png"}},
				{name = "Strong Mana Cask", thingId = 28566, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Strong_Mana_Cask.png"}},
				{name = "Great Mana Cask", thingId = 28567, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 4, icons = {"Great_Mana_Cask.png"}},
				{name = "Ultimate Mana Cask", thingId = 28568, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 5, icons = {"Ultimate_Mana_Cask.png"}},
				{name = "Great Spirit Cask", thingId = 28575, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 3, icons = {"Great_Spirit_Cask.png"}},
				{name = "Ultimate Spirit Cask", thingId = 28576, count = 1000, type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, price = 6, icons = {"Ultimate_Spirit_Cask.png"}},
				{name = "Vengothic Chair", description = "Buy an incredible Vengothic Chair to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27897, count = 1, price = 3, icons = {"Product_HouseEquipment_VengothicFurniture_Chair.png"}},
				{name = "Vengothic Chest", description = "Buy an incredible Vengothic Chest to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27905, count = 1, price = 3, icons = {"Product_HouseEquipment_VengothicFurniture_Chest.png"}},
				{name = "Vengothic Cabinet", description = "Buy an incredible Vengothic Cabinet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27903, count = 1, price = 3, icons = {"Product_HouseEquipment_VengothicFurniture_Cupboard.png"}},
				{name = "Vengothic Table", description = "Buy an incredible Vengothic Table to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27901, count = 1, price = 3, icons = {"Product_HouseEquipment_VengothicFurniture_Table.png"}},
				{name = "Vengothic Lamp", description = "Buy an incredible Vengothic Lamp to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27886, count = 1, price = 2, icons = {"Product_HouseEquipment_VengothicLamp.png"}},
				{name = "Chamaleon", description = "Buy an incredible Chamaleon to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27889, count = 1, price = 8, icons = {"Product_HouseEquipment_Chameleon.png"}},
				{name = "Blooming Cactus", description = "Buy an incredible Blooming Cactus to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27892, count = 1, price = 1, icons = {"Product_HouseEquipment_BloomingCactus.png"}},
				{name = "Bitter-Smack Leaf", description = "Buy an incredible Bitter-Smack Leaf to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27893, count = 1, price = 1, icons = {"Product_HouseEquipment_BitterSmackLeaf.png"}},
				{name = "Pink Roses", description = "Buy an incredible Pink Roses to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27894, count = 1, price = 1, icons = {"Product_HouseEquipment_PinkRoses.png"}},
				{name = "Red Roses", description = "Buy an incredible Red Roses to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27895, count = 1, price = 1, icons = {"Product_HouseEquipment_RedRoses.png"}},
				{name = "Yellow Roses", description = "Buy an incredible Yellow Roses to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 2789, count = 1, price = 1, icons = {"Product_HouseEquipment_YellowRoses.png"}},
				{name = "Parrot", description = "Buy an incredible Parrot to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27100, count = 1, price = 8, icons = {"Product_HouseEquipment_Housepet_Parrot.png"}},
				{name = "Skull Lamp", description = "Buy an incredible Skull Lamp to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27102, count = 1, price = 2, icons = {"Product_HouseEquipment_Lamp_Skull.png"}},
				{name = "Flowery Carpet", description = "Buy an incredible Flowery Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27084, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet_10.png"}},
				{name = "Colourful Carpet", description = "Buy an incredible Colourful Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27085, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet_11.png"}},
				{name = "Striped Carpet", description = "Buy an incredible Striped Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27086, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet_12.png"}},
				{name = "Patterned Carpet", description = "Buy an incredible Patterned Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27089, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet_15.png"}},
				{name = "Fur Carpet", description = "Buy an incredible Fur Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27087, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_13.png"}},
				{name = "Diamond Carpet", description = "Buy an incredible Diamond Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27088, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet_14.png"}},
				{name = "Night Sky Carpet", description = "Buy an Night Sky Carpet Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27090, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet_16.png"}},
				{name = "Star Carpet", description = "Buy an incredible Star Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27091, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_17.png"}},
				{name = "Gilded Imbuing Shrine", description = "Buy an incredible Gilded Imbuing Shrine to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27850, count = 1, price = 10, icons = {"Product_HouseEquipment_GildedImbuingShrine.png"}},
				--{name = "Imbuing Shrine", description = "Buy an incredible Imbuing Shrine to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 27728, count = 1, price = 150, icons = {"Product_HouseEquipment_ImbuingShrine.png"}}, bugged, can't move or rotate
				{name = "Fish Tank", description = "Buy an incredible Fish Tank to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26347, count = 1, price = 8, icons = {"Product_HouseEquipment_Housepet_FishTank.png"}},
				{name = "Dog House", description = "Buy an incredible Dog House to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26364, count = 1, price = 8, icons = {"Product_HouseEquipment_Housepet_DogHouse.png"}},
				{name = "Golden Dragon Tapestry", description = "Buy an incredible Golden Dragon Tapestry to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26379, count = 1, price = 2, icons = {"Product_HouseEquipment_Tapestry_04.png"}},
				{name = "Sword Tapestry", description = "Buy an incredible Sword Tapestry to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26380, count = 1, price = 2, icons = {"Product_HouseEquipment_Tapestry_05.png"}},
				{name = "Brocade Tapestry", description = "Buy an incredible Brocade Tapestry to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26381, count = 1, price = 2, icons = {"Product_HouseEquipment_Tapestry_06.png"}},
				{name = "Rustic Cabinet", description = "Buy an incredible Rustic Cabinet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26356, count = 1, price = 3, icons = {"Product_HouseEquipment_RusticFurniture_Cabinet.png"}},
				{name = "Rustic Chair", description = "Buy an incredible Rustic Chair to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26349, count = 1, price = 3, icons = {"Product_HouseEquipment_RusticFurniture_Chair.png"}},
				{name = "Rustic Trunk", description = "Buy an incredible Rustic Trunk to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26358, count = 1, price = 3, icons = {"Product_HouseEquipment_RusticFurniture_Chest.png"}},
				{name = "Rustic Table", description = "Buy an incredible Rustic Table to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26354, count = 1, price = 3, icons = {"Product_HouseEquipment_RusticFurniture_Table.png"}},
				{name = "Crimson Carpet", description = "Buy an incredible Crimson Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26371, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_04.png"}},
				{name = "Azure Carpet", description = "Buy an incredible Azure Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26372, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_05.png"}},
				{name = "Emerald Carpet", description = "Buy an incredible Emerald Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26373, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_06.png"}},
				{name = "Light Parquet", description = "Buy an incredible Light Parquet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26374, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_07.png"}},
				{name = "Dark Parquet", description = "Buy an incredible Dark Parquet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26375, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_08.png"}},
				{name = "Marble Floor", description = "Buy an incredible Marble Floor to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26370, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet_09.png"}},
				{name = "Baby Dragon", description = "Buy an incredible Baby Dragon to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26098, count = 1, price = 8, icons = {"Product_HouseEquipment_Housepet_BabyDragon.png"}},
				{name = "Cat in a Basket", description = "Buy an incredible Cat in a Basket to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26107, count = 1, price = 8, icons = {"Product_HouseEquipment_Housepet_Cat.png"}},
				{name = "Hamster in a Wheel", description = "Buy an incredible Hamster in a Wheel to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26100, count = 1, price = 8, icons = {"Product_HouseEquipment_Housepet_Hamster.png"}},
				{name = "Magnificent Cabinet", description = "Buy an incredible Magnificent Cabinet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26075, count = 1, price = 3, icons = {"Product_HouseEquipment_BaroqueFurniture_Cabinet.png"}},
				{name = "Magnificent Chair", description = "Buy an incredible Magnificent chair to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26059, count = 1, price = 3, icons = {"Product_HouseEquipment_BaroqueFurniture_Chair.png"}},
				{name = "Magnificent Trunk", description = "Buy an incredible Magnificent Trunk to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26083, count = 1, price = 3, icons = {"Product_HouseEquipment_BaroqueFurniture_Chest.png"}},
				{name = "Magnificent Table", description = "Buy an incredible Magnificent Table to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26073, count = 1, price = 3, icons = {"Product_HouseEquipment_BaroqueFurniture_Table.png"}},
				{name = "Ferocious Cabinet", description = "Buy an incredible Ferocious Cabinet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26077, count = 1, price = 3, icons = {"Product_HouseEquipment_TortureChamberFurniture_Cabinet.png"}},
				{name = "Ferocious Chair", description = "Buy an incredible Ferocious Chair to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26063, count = 1, price = 3, icons = {"Product_HouseEquipment_TortureChamberFurniture_Chair.png"}},
				{name = "Ferocious Trunk", description = "Buy an incredible Ferocious Trunk to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26079, count = 1, price = 3, icons = {"Product_HouseEquipment_TortureChamberFurniture_Chest.png"}},
				{name = "Ferocious Table", description = "Buy an incredible Ferocious Table to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26067, count = 1, price = 3, icons = {"Product_HouseEquipment_TortureChamberFurniture_Table.png"}},
				{name = "Yalaharian Carpet", description = "Buy an incredible Yalaharian Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26109, count = 1, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet1.png"}},
				{name = "White Fur Carpet", description = "Buy an incredible White Fur Carpet to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26110, count = 1, price = 2, icons = {"Product_HouseEquipment_Carpet2.png"}},
				{name = "Bamboo Mat", description = "Buy an incredible Bamboo Mat to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 2611, count = 26111, price = 2, icons = {"Malvera_Product_HouseEquipment_Carpet3.png"}},
				{name = "Lit Protectress Lamp", description = "Buy an incredible Protectress Lamp to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26094, count = 1, price = 1, icons = {"Product_HouseEquipment_Lamp_Goddess.png"}},
				{name = "Lit Predator Lamp", description = "Buy an incredible Predator Lamp to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26090, count = 1, price = 1, icons = {"Product_HouseEquipment_Lamp_Leopard.png"}},
				{name = "Royal Mailbox", description = "Buy an incredible Royal Mailbox to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26055, count = 1, price = 10, icons = {"Product_HouseEquipment_Mailbox_Golden.png"}},
				{name = "Ornate Mailbox", description = "Buy an incredible Ornate Mailbox to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26057, count = 1, price = 8, icons = {"Product_HouseEquipment_Mailbox_Standard.png"}},
				{name = "Lordly Tapestry", description = "Buy an incredible Lordly Tapestry to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26104, count = 1, price = 2, icons = {"Product_HouseEquipment_Tapestry_01.png"}},
				{name = "Menacing Tapestry", description = "Buy an incredible Menacing Tapestry to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26105, count = 1, price = 2, icons = {"Product_HouseEquipment_Tapestry_02.png"}},
				{name = "All-Seeing Tapestry", description = "Buy an incredible All-Seeing Tapestry to decorate your home.", type = GameStore.OfferTypes.OFFER_TYPE_HOUSE, thingId = 26106, count = 1, price = 2, icons = {"Product_HouseEquipment_Tapestry_03.png"}},

	    }
	},
	
}

-- Non-Editable
local runningId = 1
for k, category in ipairs(GameStore.Categories) do
	if category.offers then
		for m, offer in ipairs(category.offers) do
			offer.id = runningId
			runningId = runningId + 1
			
			if not offer.type then
				offer.type = GameStore.OfferTypes.OFFER_TYPE_NONE
			end
		end
	end
end
